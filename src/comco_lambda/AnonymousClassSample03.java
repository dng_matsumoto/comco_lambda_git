/**
 *
 */
package comco_lambda;

/**
 * @author Y.Matsumoto
 *
 */
public class AnonymousClassSample03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/**
		 * Runnableのインスタンスを生成しているように見えるけど
		 * 実は名無しのクラスを定義すると同時にインスタンス化している
		 */
		Runnable runner = new Runnable() {
			public void run() {
				System.out.println("Hello Lambda!");
			}
		};

		//実行
		runner.run(); //Hello Lambda!
	}

}
