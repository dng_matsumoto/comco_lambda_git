/**
 *
 */
package comco_lambda;

/**
 * @author Y.Matsumoto
 *
 */
public class BasicLambda04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Runnable runner = () -> {
			System.out.println("Hello Lambda!");
		};
		runner.run(); //Hello Lambda!
	}
}
