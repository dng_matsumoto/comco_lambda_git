/**
 *
 */
package comco_lambda;

import inf.Something;

/**
 * @author Y.Matsumoto
 *
 */
public class FunctionalInterface05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//内部クラスを利用する
		class Test01 implements Something {

			@Override
			public int execute(String data) {
				return data.length();
			}

		}

		Something something01 = new Test01();
		System.out.println(something01.execute("ABC"));


		//無名クラスを利用する
		Something something02 = new Something() {

			@Override
			public int execute(String data) {
				return data.length();
			}
		};

		System.out.println(something02.execute("ABCD"));



		//ラムダ式を利用する
		Something something03 = (String data) -> {
			return data.length();
		};
		System.out.println(something03.execute("ABCDE"));


		//ラムダ式を利用する(引数が１つなので型とかっこを省略　returnと波かっこも省略)
		Something something04 = data -> data.length();
		System.out.println(something04.execute("ABCDEF"));

	}

}
