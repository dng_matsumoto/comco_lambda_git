/**
 *
 */
package comco_lambda;

import java.util.function.Predicate;

/**
 * 文字の長さが１０文字未満の時にtrueになるメソッドをいろいろな方法で作る
 * @author Y.Matsumoto
 *
 */
public class FunctionalInterfaceOnApi06 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//内部クラスを利用する
		class Test01 implements Predicate<String> {

			@Override
			public boolean test(String t) {
				return t.length() < 10;
			}


		}

		Predicate<String> something01 = new Test01();
		System.out.println(something01.test("ABC"));
		System.out.println(something01.test("ABCDEFGHIJK"));
		System.out.println("-----------------------------------------");

		//無名クラスを利用する
		Predicate<String> something02 = new Predicate<String>() {

			@Override
			public boolean test(String t) {

				return t.length() < 10;
			}

		};

		System.out.println(something02.test("ABC"));
		System.out.println(something02.test("ABCDEFGHIJK"));
		System.out.println("-----------------------------------------");


		//ラムダ式を利用する
		Predicate<String> something03 = (String data) -> {
			return data.length() < 10;
		};
		System.out.println(something03.test("ABCDE"));
		System.out.println(something03.test("ABCDEFGHIJK"));
		System.out.println("-----------------------------------------");


		//ラムダ式を利用する(引数が１つなので型とかっこを省略　returnと波かっこも省略)
		Predicate<String> something04 = data -> data.length() < 10;
		System.out.println(something04.test("ABCDEF"));
		System.out.println(something04.test("ABCDEFGHIJK"));

	}

}
