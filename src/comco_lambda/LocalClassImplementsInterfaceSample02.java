/**
 *
 */
package comco_lambda;

/**
 * @author Y.Matsumoto
 *
 */
public class LocalClassImplementsInterfaceSample02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		/**
		 * インターフェースを取り込んだローカルクラス
		 * Runnableを取り込んでスレッドになる
		 * @author Y.Matsumoto
		 *
		 */
		class Local implements Runnable {
			public void run() {
				System.out.println("Hello Lambda!");
			}
		}

		//インスタンス化してスレッドとして実行
		Runnable runner = new Local();
		runner.run(); // Hello Lambda!

	}

}
