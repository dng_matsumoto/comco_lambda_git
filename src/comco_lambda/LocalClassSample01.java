/**
 *
 */
package comco_lambda;

/**
 * @author Y.Matsumoto
 *
 */
public class LocalClassSample01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String msg = "Guten Tag";

		/**
		 * ローカルクラス
		 * このメソッド内でのみ有効なクラス
		 * @author Y.Matsumoto
		 *
		 */
		class Local {
			public void sayHello() {
				System.out.println(msg);
			}
		}

		//ローカルクラスをインスタンス化して、インスタンスメソッドを実行
		Local local = new Local();
		local.sayHello();
	}

}
