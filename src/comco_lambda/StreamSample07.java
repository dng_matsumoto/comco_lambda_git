/**
 *
 */
package comco_lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Y.Matsumoto
 *
 */
public class StreamSample07 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//ArrayList numTextListに入っている数値化可能な文字列をnumListに数値化して格納する
		List<String> numTextList = Arrays.asList("0", "1", null);

		// for版
		List<Integer> numListFor = new ArrayList<>();
		for (int i = 0; i < numTextList.size(); i++) {
			String numText = numTextList.get(i);
			if (numText == null) {
				continue;
			}
			final int num = Integer.parseInt(numText);
			numListFor.add(num);
		}

		// 拡張for版
		List<Integer> numListExtendFor = new ArrayList<>();
		for (String numText : numTextList) {
			if (numText == null) {
				continue;
			}
			final int num = Integer.parseInt(numText);
			numListExtendFor.add(num);
		}

		//Stream 版
		final List<Integer> numListStream01 = numTextList.stream()
				.filter((String numText) -> {
					return Objects.nonNull(numText);
				})
				.map((String numText) -> {
					return Integer.parseInt(numText);
				})
				.collect(Collectors.toList());

		final List<Integer> numListStream02 = numTextList.stream()
				.filter((numText) -> {
					return Objects.nonNull(numText);
				})
				.map((numText) -> {
					return Integer.parseInt(numText);
				})
				.collect(Collectors.toList());

		final List<Integer> numListStream03 = numTextList.stream()
				.filter(numText -> {
					return Objects.nonNull(numText);
				})
				.map(numText -> {
					return Integer.parseInt(numText);
				})
				.collect(Collectors.toList());

		final List<Integer> numListStream04 = numTextList.stream()
				.filter(numText -> Objects.nonNull(numText))
				.map(numText -> Integer.parseInt(numText))
				.collect(Collectors.toList());

		final List<Integer> numListStream05 = numTextList.stream()
				.filter(s -> Objects.nonNull(s))
				.map(s -> Integer.parseInt(s))
				.collect(Collectors.toList());

		final List<Integer> numListStream06 = numTextList.stream()
				.filter(Objects::nonNull)
				.map(Integer::parseInt)
				.collect(Collectors.toList());

		final List<Integer> numList = numTextList.stream().filter(Objects::nonNull).map(Integer::parseInt)
				.collect(Collectors.toList());
	}

}
