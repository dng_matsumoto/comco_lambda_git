package inf;


/**
 * 関数型インターフェイスの例
 * @author Y.Matsumoto
 *
 */
@FunctionalInterface
public interface Something {

	//条件：抽象メソッドが１つ
	int execute(String data);

	//このコメントを外すとコンパクトエラー
//	boolean test(String data);
}
