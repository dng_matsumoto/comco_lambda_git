package lambda01;

import java.util.Arrays;
import java.util.List;

/**
 * @author IshiiMasato
 *
 */
public class Lambda {

	public static void main(String[] args) {
		List<Integer> numList = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		for(int i=0; i < numList.size();i++) {
			if(numList.get(i) % 2 == 0) {
				System.out.println(numList.get(i));
			}

		}
	}

}
