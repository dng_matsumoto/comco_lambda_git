/**
 *
 */
package lambda05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 岡田隼汰
 *
 */
public class Lambda {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int a : array) {
			if (a % 2 == 0) {
				System.out.println(a);
			}
		}

		List<Integer> list = new ArrayList<>(array.length);
		for (int i = 0; i < array.length; i++) {
		    list.add(array[i]);
		}
		Arrays.asList(list).forEach(num -> System.out.println(num));
	}
}
