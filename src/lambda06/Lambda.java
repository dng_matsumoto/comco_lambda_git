/**
 *
 */
package lambda06;

import java.util.ArrayList;
import java.util.List;

/**
 * @author takahiro kawashima
 *
 */
public class Lambda {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		List<Integer> numList = new ArrayList<>();

		for (int i = 0; i < array.length; i++) {
			int num = array[i];
			if (num % 2 == 0) {
				System.out.println(num);
			}
		}
	}

}
