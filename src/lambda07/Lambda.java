/**
 *
 */
package lambda07;

import java.util.Arrays;
import java.util.List;

/**
 * @author sato kazusa
 *
 */
public class Lambda {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//仕様通り
		//int型配列に1~10の値を格納
		int [] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

		//偶数のみ表示
		for(int i : array) {
			if(i % 2 == 0) {
				System.out.println(i);
			}
		}


		//ArrayList挑戦版
		//1~10までの整数が格納されたArrayList
		List<Integer> intList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

		//偶数の要素だけを表示させる(通常のfor)
		for(int i = 0; i < intList.size(); i++) {
			if(intList.get(i) % 2 == 0) {
				System.out.println(intList.get(i));
			}
		}

		//偶数の要素だけを表示させる(拡張for)
		for(int i : intList) {
			if(i % 2 == 0) {
				System.out.println(i);
			}
		}
	}

}
