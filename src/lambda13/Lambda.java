/**
 *
 */
package lambda13;

/**
 * @author Tsubaki Arisu
 *
 */
public class Lambda {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] array= {1,2,3,4,5,6,7,8,9,10 };
		//int[] array= {1,3,7,9};
		int count=0;

		System.out.print("偶数は ");
		for(int kazu:array) {
			if(kazu%2==0||kazu==0) {
				System.out.print(kazu+" ");
			}else {
				count++;
			}
		}
		if(array.length==count) {
			System.out.println("ありませんでした");
		}
	}
}
