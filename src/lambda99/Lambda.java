/**
 *
 */
package lambda99;

import java.util.Arrays;
import java.util.List;

/**
 * @author Y.Matsumoto
 *
 */
public class Lambda {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		list.stream().filter(i -> i % 2 == 0).forEach(System.out::println);
	}

}
